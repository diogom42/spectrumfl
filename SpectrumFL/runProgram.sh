#!/bin/bash

#parametros
#dir, programa, i+1
lcov -z -d $1 -q
./"$2".exe < inputs/input"$3" > outputs/out"$3"
lcov -c -d $1 -o traces/trace"$3".info --test-name trace"$3" -q
