#!/bin/bash

#parametros
#dir, programa
#compila programa criando variaveis de instrumentalizacao
g++ -fprofile-arcs -ftest-coverage "$2" -o "$2".exe

#modifica permicao para o executavel
#chmod u+x "$2".exe

#gera arquivo de trace vazio
lcov -c -i -d "$1" -o "$1"app_base.info --test-name base -q

#cria diretorios necessarios
mkdir inputs outputs traces
rm inputs/* outputs/* traces/*
