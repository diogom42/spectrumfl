package org.element.fourth.engine.covData;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by diogo on 23/06/17.
 */
public class covData {
    /**
     * Executa scripts compileProgram.sh e runProgram.sh.
     * compileProgram.sh compila o programa criando as variáveis de instrumentalização e runProgram executa os casos de teste e cria os traces.
     * São criados três diretórios "inputs", "outputs" e "traces".
     * "inputs" contém os arquivos com as entradas dos casos de testes.
     * "outputs" contém as saídas dos casos de teste.
     * "traces" contém o trace gerado pela ferramenta lcov.
     * @param program nome do programa (do arquivo .cpp)
     * @param Tests ArrayList que contém os casos de teste
     */
    public static void runProgram(String program, ArrayList<ArrayList> Tests) {
        String s = null;
        String dir = null;

        try {
            //carrega diretorio atual com comando pwd
            BufferedReader pwd = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("pwd").getInputStream()));
            dir = pwd.readLine() + "/";

            System.out.println("./compileProgram.sh " + dir + " " + program);
            Process p = Runtime.getRuntime().exec("./compileProgram.sh " + dir + " " + program);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            // read the output from the command
            if ((s = stdInput.readLine()) != null) {
                System.out.println("Output compileProgram.sh:\n");
                System.out.println(s);
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                }
            }

            // read any errors from the attempted command
            if ((s = stdError.readLine()) != null) {
                System.out.println("Error Message compileProgram.sh:\n");
                System.out.println(s);
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                }
            }

            for (int i = 0; i < Tests.get(0).size(); i++) {//Tests.get(0).size()
                //Runtime.getRuntime().exec("echo " + Tests.get(0).get(i) + " > teste.tes");

                PrintWriter writer = new PrintWriter("inputs/input" + (i + 1), "UTF-8");
                writer.println(Tests.get(0).get(i));
                writer.close();
            }

            for (int i = 0; i < Tests.get(0).size(); i++) {
                //System.out.println("./runProgram.sh " + dir + " " + program + " " + (i+1));
                p = Runtime.getRuntime().exec("./runProgram.sh " + dir + " " + program + " " + (i+1));
                stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                // read the output from the command
                if ((s = stdInput.readLine()) != null) {
                    System.out.println("Output compileProgram.sh:\n");
                    System.out.println(s);
                    while ((s = stdInput.readLine()) != null) {
                        System.out.println(s);
                    }
                }
                // read any errors from the attempted command
                if ((s = stdError.readLine()) != null) {
                    System.out.println("Error Message compileProgram.sh:\n");
                    System.out.println(s);
                    while ((s = stdError.readLine()) != null) {
                        System.out.println(s);
                    }
                }
            }
        }
        catch (IOException e) {
            System.err.println("Eita, deu ruim!");
            e.printStackTrace();
        }
    }

    /**
     * Retorna lista de testes negativos (indíces em Tests)
     * @param Tests ArrayList com casos de teste
     * @return ArrayList com indíces dos casos de teste negativos
     */
    public static ArrayList getFaults(ArrayList<ArrayList> Tests) {
        ArrayList faults = new ArrayList();
        try {
            for (int i = 0; i < Tests.get(1).size(); i++) {
                String content = readFile("outputs/out" + (i+1), Charset.defaultCharset());
                if (!content.equals(Tests.get(1).get(i))) {
                    faults.add(i);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return faults;
    }

    /**
     * Retorna String com o conteúdo de um arquivo
     * @param path caminho do arquivo de texto
     * @param encoding codificação do arquivo
     * @return String que contém o texto do arquivo
     * @throws IOException
     */
    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * Retorna ArrayList de matriz de cobertura
     * @param testCases Quantidade de casos de teste
     * @return
     */
    public static ArrayList<ArrayList> getCovMat(int testCases) {
        ArrayList<ArrayList> covMat = new ArrayList();

        BufferedReader inputFileBR = null;
        FileReader inputFileFR = null;


        for (int i = 0; i < testCases; i++) {
            //cada linha do arquivo (caso de teste) inputs executa este bloco
            covMat.add(new ArrayList());

            try {
                inputFileFR = new FileReader("traces/trace" + (i+1) + ".info");
                inputFileBR = new BufferedReader(inputFileFR);

                String sCurrentLine;

                inputFileBR = new BufferedReader(new FileReader("traces/trace" + (i+1) + ".info"));

                while ((sCurrentLine = inputFileBR.readLine()) != null) {
                    //busca as linhas com informacao de cobertura
                    //linhas seguem o formato DA:<line number>,<execution count>

                    if (sCurrentLine.substring(0, 3).equals("DA:")) {
                        if (Integer.parseInt(sCurrentLine.substring(sCurrentLine.indexOf(',')+1, sCurrentLine.length())) > 0) {
                            covMat.get(i).add(1);
                        } else {
                            covMat.get(i).add(0);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputFileBR != null)
                        inputFileBR.close();
                    if (inputFileFR != null)
                        inputFileFR.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return covMat;
    }

    /**
     * Retorna ArrayList dos indices das linhas instrumentalizadas
     * @return
     */
    public static ArrayList getInstLines() {
        ArrayList instLines = new ArrayList();

        BufferedReader inputFileBR = null;
        FileReader inputFileFR = null;

        try {
            inputFileFR = new FileReader("app_base.info");
            inputFileBR = new BufferedReader(inputFileFR);

            String sCurrentLine;

            while ((sCurrentLine = inputFileBR.readLine()) != null) {
                //busca as linhas com informacao de cobertura
                //linhas seguem o formato DA:<line number>,<execution count>

                if (sCurrentLine.substring(0, 3).equals("DA:")) {
                    instLines.add(sCurrentLine.substring(3, sCurrentLine.indexOf(',')));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputFileBR != null)
                    inputFileBR.close();
                if (inputFileFR != null)
                    inputFileFR.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }


        return instLines;
    }

}
