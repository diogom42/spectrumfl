package org.element.fourth.engine.covData;

import java.util.ArrayList;

public class Spectrum {

    public int [] qtdFailed;//quantidade de casos de teste negativos que cobrem cada elemento
    public int [] qtdSuccessful;//quantidade de casos de teste positivos que cobrem cada elemento
    public int tests;//numero de casos de teste
    public int sTests, fTests;//numero de casos de teste positivos e negativos
	public int elements;//n de linhas instrumentalizadas no programa
    public ArrayList instLines;

	public Spectrum(ArrayList<ArrayList> Tests) {
		ArrayList faults = covData.getFaults(Tests);
		ArrayList<ArrayList> covMat = covData.getCovMat(Tests.get(0).size());

		this.tests = Tests.get(0).size();
		this.fTests = faults.size();
		this.sTests = tests - fTests;
		this.elements = covMat.get(0).size();
        this.instLines = covData.getInstLines();

        this.qtdFailed = new int[elements];//vetor com o numero de casos de teste negativos que executecutam cada elemento
        this.qtdSuccessful = new int[elements];//vetor com o numero de casos de teste positivos que executecutam cada elemento

        for (int i = 0; i < tests; i++) {
            if (faults.contains(i)) {
                for (int j = 0; j < elements; j++) {
                    qtdFailed[j] += (int) covMat.get(i).get(j);
                }
            } else {
                for (int j = 0; j < elements; j++) {
                    qtdSuccessful[j] += (int) covMat.get(i).get(j);
                }
            }
        }
		
	}

	/**
	 * Calcula a suspeita de todos os elementos pela formula Ochiai
	 * @return Matriz de com numero da linha instrumentalizada e a suspeita calculada.
	 */
	public ArrayList<ArrayList> Ochiai() {
		ArrayList<ArrayList> Suspiciousness = new ArrayList();
		ArrayList<ArrayList> instLines = covData.getInstLines();

		for (int e = 0; e < elements; e++) {
			Suspiciousness.add(new ArrayList());
			Suspiciousness.get(e).add(instLines.get(e));
			Suspiciousness.get(e).add(qtdFailed[e]/Math.sqrt(sTests * (((qtdFailed[e] == 0) ? Double.MIN_VALUE : qtdFailed[e]) + ((qtdSuccessful[e] == 0) ? Double.MIN_VALUE : qtdSuccessful[e]))));
		}

		return Suspiciousness;
	}
}