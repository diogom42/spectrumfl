package org.element.fourth.engine;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.element.fourth.engine.covData.Spectrum;
import org.element.fourth.engine.covData.covData;

import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.element.fourth.engine.covData.covData.runProgram;

public class Main {

    public static void main(String[] args) {

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader("test.json"));

            JSONObject jsonObject = (JSONObject) obj;


            String dados = (String) jsonObject.get("dados");

            String program = (String) jsonObject.get("arquivo");

            String casoTeste = (String) jsonObject.get("casoTeste");

            String path = null;
            ArrayList<ArrayList> Tests = new ArrayList();

            byte[] rar = Base64.decodeBase64(dados);
            try {
                String s = null;

                //extrai arquivo .rar do bytecode
                FileUtils.writeByteArrayToFile(new File("file.rar"), rar);

                //descompacta arquivo .rar
                Process p = Runtime.getRuntime().exec("unrar x -y file.rar");
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                // read the output from the command
                if ((s = stdInput.readLine()) != null) {
                    System.out.println("Output unrar:\n");
                    System.out.println(s);
                    while ((s = stdInput.readLine()) != null) {
                        if (s.contains(program)) {//busca o caminho do arquivo .cpp nas saidas do programa unrar
                            Pattern pattern = Pattern.compile("\\s+(.*?)\\s+");
                            Matcher matcher = pattern.matcher(s);
                            if (matcher.find())
                            {
                                path = matcher.group(1);//guarda caminho do arquivo .cpp em path
                            }
                        }

                        System.out.println(s);
                    }
                }

                //copia os arquivos
                if (path == null) {
                    throw new IOException(program + " not found");
                }
                Runtime.getRuntime().exec("cp " + path + " " + program);

            } catch (IOException e) {
                e.printStackTrace();
            }

            Tests = parseTC(casoTeste);

            runProgram(program, Tests);//compila e executa programa com testes Tests

            Spectrum spec = new Spectrum(Tests);

            JSONObject objJSON = new JSONObject();
                objJSON.put("Ochiai", spec.Ochiai());

            try (FileWriter file = new FileWriter("out.json")) {
                file.write(objJSON.toJSONString());
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    /**
     * Parser da string de casos de teste
     * @param tc
     * @return Matriz de duas dimensões: posicao 1 com dados de entrada e posicao dois com dados de saida
     */
    public static ArrayList<ArrayList> parseTC(String tc) {
        ArrayList<ArrayList> Tests = new ArrayList();

        System.out.println(tc);
        String testcases[] = tc.split("\n");
        for (int i = 0; i < testcases.length; i++) {
            if (testcases[i].equals("<INPUT>")) {
                Tests.add(new ArrayList());
                int j;
                for (j = i + 1; !testcases[j].equals("<OUTPUT>"); j++) {
                    Tests.get(0).add(testcases[j]);
                }
                Tests.add(new ArrayList());
                for (j = j + 1; !testcases[j].equals("</TEST CASE>"); j++) {
                    Tests.get(1).add(testcases[j]);
                }
                break;
            }
        }
        System.out.println(testcases[1]);

        return Tests;
    }


}
